package ru.ferylev.gwtApp.client.ui;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import ru.ferylev.gwtApp.server.model.Book;

import java.util.HashSet;
import java.util.Set;

public class TablePanel extends Composite {

    interface TableUiBinder extends UiBinder<Widget, TablePanel> {
    }

    private static TableUiBinder ourUiBinder = GWT.create(TableUiBinder.class);

    private Set<Integer> checkedRows = new HashSet<>();

    private Column<Book, String> editButtonColumn;

    public TablePanel() {
        initWidget(ourUiBinder.createAndBindUi(this));
        initTable();
        initButtons();
    }

    @UiField
    Button addButton;

    @UiField
    Button deleteButton;

    @UiField
    CellTable<Book> table;

    private void initButtons() {
        addButton.setText("Добавить");
        deleteButton.setText("Удалить");
    }

    private void initTable() {
        Column<Book, Boolean> checkColumn = new Column<Book, Boolean>(new CheckboxCell(true, false)) {
            @Override
            public Boolean getValue(Book checkedBook) {
                return false;
            }
        };
        checkColumn.setFieldUpdater((index, book, checked) -> changeSelection(book, checked));

        TextColumn<Book> nameColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book book) {
                return book.getBookName();
            }
        };
        TextColumn<Book> authorColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book book) {
                return book.getAuthor();
            }
        };
        TextColumn<Book> genreColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book book) {
                return book.getGenre();
            }
        };
        TextColumn<Book> yearColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book book) {
                return book.getPubYear();
            }
        };

        ButtonCell editButtonCell = new ButtonCell();
        editButtonColumn = new Column<Book, String>(editButtonCell) {
            @Override
            public String getValue(Book book) {
                return "Редактировать";
            }
        };

        table.addColumn(checkColumn);
        table.addColumn(nameColumn, "Название");
        table.addColumn(authorColumn, "Автор");
        table.addColumn(genreColumn, "Жанр");
        table.addColumn(yearColumn, "Год публикации");
        table.addColumn(editButtonColumn, "");
    }

    private void changeSelection(Book selectedRow, Boolean checked) {
        if (checked) {
            checkedRows.add(selectedRow.getId());
        } else {
            checkedRows.remove(selectedRow.getId());
        }
    }

    public Set<Integer> getCheckedRows() {
        return checkedRows;
    }

    public CellTable<Book> getTable() {
        return table;
    }

    public Button getAddButton() {
        return addButton;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public Column<Book, String> getEditButtonColumn() {
        return editButtonColumn;
    }
}