package ru.ferylev.gwtApp.client.ui;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class ModalPanel extends Composite {

    interface ModalPanelUiBinder extends UiBinder<Widget, ModalPanel> {
    }

    private static ModalPanelUiBinder ourUiBinder = GWT.create(ModalPanelUiBinder.class);

    public ModalPanel() {
        initWidget(ourUiBinder.createAndBindUi(this));
        closeModalButton.setText("Отменить");
    }

    @UiField
    TextBox nameTextBox;

    @UiField
    TextBox authorTextBox;

    @UiField
    TextBox genreTextBox;

    @UiField
    TextBox yearTextBox;

    @UiField
    Button closeModalButton;

    @UiField
    Button saveButton;

    public TextBox getNameTextBox() {
        return nameTextBox;
    }

    public TextBox getAuthorTextBox() {
        return authorTextBox;
    }

    public TextBox getGenreTextBox() {
        return genreTextBox;
    }

    public TextBox getYearTextBox() {
        return yearTextBox;
    }

    public Button getCloseModalButton() {
        return closeModalButton;
    }

    public Button getSaveButton() {
        return saveButton;
    }
}