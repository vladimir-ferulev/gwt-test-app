package ru.ferylev.gwtApp.client.ui;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class FilterPanel extends Composite {

    interface FilterPanelUiBinder extends UiBinder<Widget, FilterPanel> {
    }

    private static FilterPanelUiBinder ourUiBinder = GWT.create(FilterPanelUiBinder.class);

    public FilterPanel() {
        initWidget(ourUiBinder.createAndBindUi(this));
        filterButton.setText("Отфильтровать");
    }

    @UiField
    TextBox nameTextBox;

    @UiField
    TextBox authorTextBox;

    @UiField
    TextBox genreTextBox;

    @UiField
    TextBox yearTextBox;

    @UiField
    Button filterButton;


    public TextBox getNameTextBox() {
        return nameTextBox;
    }

    public TextBox getAuthorTextBox() {
        return authorTextBox;
    }

    public TextBox getGenreTextBox() {
        return genreTextBox;
    }

    public TextBox getYearTextBox() {
        return yearTextBox;
    }

    public Button getFilterButton() {
        return filterButton;
    }
}