package ru.ferylev.gwtApp.client.filter;

import ru.ferylev.gwtApp.client.ui.FilterPanel;
import ru.ferylev.gwtApp.server.model.Book;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

public class Filter implements Serializable {
    private String bookName;
    private String author;
    private String genre;
    private String pubYear;

    public Filter() {
    }

    public void setFilter(FilterPanel filterPanel) {
        this.bookName = filterPanel.getNameTextBox().getText().toLowerCase();
        this.author = filterPanel.getAuthorTextBox().getText().toLowerCase();
        this.genre = filterPanel.getGenreTextBox().getText().toLowerCase();
        this.pubYear = filterPanel.getYearTextBox().getText().toLowerCase();
    }

    public List<Book> filterBooks(List<Book> allBooks) {
        return allBooks.stream()
                .filter(book -> bookName.isEmpty() || book.getBookName().toLowerCase().contains(bookName))
                .filter(book -> author.isEmpty() || book.getAuthor().toLowerCase().contains(author))
                .filter(book -> genre.isEmpty() || book.getGenre().toLowerCase().contains(genre))
                .filter(book -> pubYear.isEmpty() || book.getPubYear().toLowerCase().contains(pubYear))
                .collect(Collectors.toList());
    }
}
