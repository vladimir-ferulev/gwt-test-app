package ru.ferylev.gwtApp.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.ferylev.gwtApp.server.model.Book;

import java.util.Collection;
import java.util.Set;

public interface CatalogServiceAsync {
    void getList(AsyncCallback<Collection<Book>> async);

    void save(Book editedBook, AsyncCallback<Void> async);

    void delete(Collection<Integer> checkedRows, AsyncCallback<Void> async);
}
