package ru.ferylev.gwtApp.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.view.client.ListDataProvider;
import ru.ferylev.gwtApp.client.filter.Filter;
import ru.ferylev.gwtApp.client.ui.FilterPanel;
import ru.ferylev.gwtApp.client.ui.ModalPanel;
import ru.ferylev.gwtApp.client.ui.TablePanel;
import ru.ferylev.gwtApp.server.model.Book;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GwtApp implements EntryPoint {

    private CatalogServiceAsync catalogService = GWT.create(CatalogService.class);

    private HorizontalPanel mainPanel = new HorizontalPanel();

    private TablePanel tablePanel = new TablePanel();
    private DialogBox dialogBox = new DialogBox();
    private ModalPanel modalPanel = new ModalPanel();

    private FilterPanel filterPanel = new FilterPanel();
    private Filter filter = new Filter();

    private ListDataProvider<Book> dataProvider = new ListDataProvider<>();
    private List<Book> allBooks = new ArrayList<>();
    private Book bookForm = new Book();

    public void onModuleLoad() {
        buildUserInterface();
        addHandlers();
        updateTableData();
    }

    private void buildUserInterface() {
        mainPanel.add(filterPanel);
        mainPanel.add(tablePanel);
        dialogBox.add(modalPanel);
        dataProvider.addDataDisplay(tablePanel.getTable());
        RootPanel.get().add(mainPanel);
    }

    private void addHandlers() {
        tablePanel.getAddButton().addClickHandler(clickEvent -> openModalForAdd());

        tablePanel.getDeleteButton().addClickHandler(clickEvent -> deleteSelectedBooks());

        tablePanel.getEditButtonColumn().setFieldUpdater((index, editedBook, value) -> {
            bookForm = editedBook;
            openModalForEdit(bookForm);
        });

        tablePanel.getTable().addDomHandler(event -> {
            Integer rowIndex = ((CellTable) event.getSource()).getKeyboardSelectedRow();
            bookForm = dataProvider.getList().get(rowIndex);
            openModalForEdit(bookForm);
        }, DoubleClickEvent.getType());

        modalPanel.getSaveButton().addClickHandler(clickEvent -> saveBook());

        modalPanel.getCloseModalButton().addClickHandler(clickEvent -> dialogBox.hide());

        filterPanel.getFilterButton().addClickHandler(clickEvent -> filterTable());
    }

    private void updateTableData() {
        catalogService.getList(new AsyncCallback<Collection<Book>>() {
            @Override
            public void onFailure(Throwable throwable) {
                GWT.log("Failed to get book list");
            }

            @Override
            public void onSuccess(Collection<Book> books) {
                dataProvider.getList().clear();
                dataProvider.getList().addAll(books);
                allBooks.clear();
                allBooks.addAll(books);
            }
        });
        tablePanel.getCheckedRows().clear();
    }

    private void saveBook() {
        bookForm.setBookName(modalPanel.getNameTextBox().getText());
        bookForm.setAuthor(modalPanel.getAuthorTextBox().getText());
        bookForm.setGenre(modalPanel.getGenreTextBox().getText());
        bookForm.setPubYear(modalPanel.getYearTextBox().getText());
        if ("Добавить".equals(modalPanel.getSaveButton().getText().trim())) {
            bookForm.setId(null);
        }
        catalogService.save(bookForm, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable throwable) {
                GWT.log("Failed to save book");
            }

            @Override
            public void onSuccess(Void aVoid) {
                dialogBox.hide();
                updateTableData();
            }
        });
    }

    private void deleteSelectedBooks() {
        catalogService.delete(tablePanel.getCheckedRows(), new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable throwable) {
                GWT.log("Failed to delete books");
            }

            @Override
            public void onSuccess(Void aVoid) {
                updateTableData();
            }
        });
    }

    private void openModalForAdd() {
        modalPanel.getNameTextBox().setValue("");
        modalPanel.getAuthorTextBox().setValue("");
        modalPanel.getGenreTextBox().setValue("");
        modalPanel.getYearTextBox().setValue("");
        modalPanel.getSaveButton().setText("Добавить");

        dialogBox.setText("Добавить книгу");
        dialogBox.show();
        dialogBox.center();
    }

    private void openModalForEdit(Book editedBook) {
        modalPanel.getNameTextBox().setValue(editedBook.getBookName());
        modalPanel.getAuthorTextBox().setValue(editedBook.getAuthor());
        modalPanel.getGenreTextBox().setValue(editedBook.getGenre());
        modalPanel.getYearTextBox().setValue(editedBook.getPubYear());
        modalPanel.getSaveButton().setText("Сохранить");

        dialogBox.setText("Редактировать книгу");
        dialogBox.show();
        dialogBox.center();
    }

    private void filterTable() {
        filter.setFilter(filterPanel);
        dataProvider.setList(filter.filterBooks(allBooks));
        tablePanel.getCheckedRows().clear();
    }
}





