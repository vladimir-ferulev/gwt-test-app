package ru.ferylev.gwtApp.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import ru.ferylev.gwtApp.server.model.Book;

import java.util.Collection;

@RemoteServiceRelativePath("catalog")
public interface CatalogService extends RemoteService {
    Collection<Book> getList();

    void save(Book editedBook);

    void delete(Collection<Integer> checkedRows);
}
