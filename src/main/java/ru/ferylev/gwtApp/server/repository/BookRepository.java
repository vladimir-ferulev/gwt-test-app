package ru.ferylev.gwtApp.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.ferylev.gwtApp.server.model.Book;

import java.util.Collection;

public interface BookRepository extends JpaRepository<Book, Integer> {
    @Transactional
    void deleteByIdIn(Collection<Integer> ids);
}
