package ru.ferylev.gwtApp.server.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "books")
public class Book implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String bookName;
    private String author;
    private String genre;
    private String pubYear;

    public Book() {
    }

    public Book(String bookName, String author, String genre, String pubYear) {
        this.bookName = bookName;
        this.author = author;
        this.genre = genre;
        this.pubYear = pubYear;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getPubYear() {
        return pubYear;
    }

    public void setPubYear(String pubYear) {
        this.pubYear = pubYear;
    }
}