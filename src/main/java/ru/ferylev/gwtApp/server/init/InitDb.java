package ru.ferylev.gwtApp.server.init;

import ru.ferylev.gwtApp.server.model.Book;
import ru.ferylev.gwtApp.server.repository.BookRepository;
import ru.ferylev.gwtApp.server.service.AppContext;

import java.util.Arrays;
import java.util.List;

public class InitDb {

    private final BookRepository bookRepository = AppContext.getBean(BookRepository.class);

    private void initialData() {
        List<Book> bookList = Arrays.asList(new Book("Джейн Эйр", "Шарлотта Бронте", "Роман", "1847"),
                new Book("Дюна", "Фрэнк Герберт", "Фэнтези", "1965"),
                new Book("Гамлет", "Уильям Шекспир", "Драма", "1603"),
                new Book("Сияние", "Стивен Кинг", "Ужасы", "1977"),
                new Book("Зеленая миля", "Стивен Кинг", "Драма", "1996"),
                new Book("Анна Каренина", "Лев Толстой", "Роман", "1877"),
                new Book("Крестный отец", "Марио Пьюзо", "Драма", "1969"),
                new Book("Игра престолов", "Джордж Р.Р. Мартин", "Фэнтези", "1996"));
        bookRepository.saveAll(bookList);
    }
}
