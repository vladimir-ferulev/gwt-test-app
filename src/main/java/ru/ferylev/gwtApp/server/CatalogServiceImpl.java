package ru.ferylev.gwtApp.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.ferylev.gwtApp.client.CatalogService;
import ru.ferylev.gwtApp.server.model.Book;
import ru.ferylev.gwtApp.server.repository.BookRepository;
import ru.ferylev.gwtApp.server.service.AppContext;

import java.util.Collection;
import java.util.List;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class CatalogServiceImpl extends RemoteServiceServlet implements
        CatalogService {

    private final BookRepository bookRepository = AppContext.getBean(BookRepository.class);

    @Override
    public List<Book> getList() {
        return bookRepository.findAll();
    }

    @Override
    public void save(Book editedBook) {
        bookRepository.save(editedBook);
    }

    @Override
    public void delete(Collection<Integer> checkedRows) {
        bookRepository.deleteByIdIn(checkedRows);
    }
}
